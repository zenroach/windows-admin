﻿$file = "C:\temp\installed-software.txt"
$regkey = Get-ChildItem -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall, HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall | Get-ItemProperty | Select-Object -Property DisplayName, DisplayVersion, Publisher, InstallDate | Sort-Object DisplayName -Unique


$(ForEach($key in $regkey) {
    If($key.DisplayName) {
        $Name = $key.DisplayName.ToString()
        If ($Name -notlike '*KB*') {
           "***" 
           $key
            }
        }
    })  | format-table -HideTableHeaders | out-string -width 512 | out-file $file

(gc $file) | ?{$_.trim() -ne "" } | set-content $file 

Add-Content $file ""
Add-Content $file ""
Add-Content $file "OS:`t`t $([environment]::OSVersion)"
Add-Content $file "Version:`t $([environment]::OSVersion.Version)"