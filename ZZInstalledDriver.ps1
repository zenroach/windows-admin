﻿$drivers = Get-WmiObject Win32_PnPSignedDriver | Select devicename, driverversion, Manufacturer, installdate | Sort-Object devicename -Unique

ForEach($driver in $drivers) {
    If($driver.devicename) {
        $driver | Format-table -Wrap -Autosize
    }
}