﻿$domain = (([System.directoryservices.activedirectory.domain]::getcurrentdomain()).GetDirectoryEntry()).name.ToString() + ".com"
$users = (Get-ADUser -filter *) | sort
$(foreach ($U in $users) {
    $UN = get-aduser $U -properties primarygroup, memberof, PasswordLastSet, DisplayName

    $pgroup = (get-adgroup $UN.PrimaryGroup).name
    $groups = foreach ($group in $UN.memberof) {
        (get-adgroup $group).name
    }
    $groups = $groups | sort -Unique
    $groupString = "" 
    foreach ($group in $groups) {
       $groupstring += "|" + (get-adgroup $group).name
        }
    New-object PSObject -property @{
        Name = $UN.DisplayName
        samAccountName = $UN.SamAccountName
        Domain = $domain
        Entitlements = 
            if ($groupString -eq "") {$pgroup}
            else {$pgroup + $groupString}
        }
}) | Select-Object samAccountName,Name,Domain,Entitlements | ConvertTo-Csv -NoTypeInformation -Delimiter "," | % {$_.Replace('"','')} | out-file C:\temp\userlist.csv