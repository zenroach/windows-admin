﻿###############
## Variables ##
###############

$inputReport = "C:\Program Files\Tripwire\script\assurx\report\tripwire.xml"
$nodeListFile = "C:\Program Files\Tripwire\script\assurx\report\script\temp\nodeList.txt"
$assurx = "C:\Program Files\Tripwire\script\assurx\report\script\report\assurx.xml"
$temp = "C:\Program Files\Tripwire\script\assurx\report\script\temp"
$backup = "C:\Program Files\Tripwire\script\assurx\report\script\backup"

######################
## Gather Node List ##
######################

[xml]$elementReport = Get-Content $inputReport
$nodeList = $elementReport.ReportOutput.ReportBody.ReportSection | Select name | foreach-object{$_.name} | Out-File $nodeListFile
Write-Host "** Finishing Gathering Node List **"

####################
## Begin XML File ##
####################

"<?xml version=`"1.0`" encoding=`"UTF-8`"?>`n<Report Name=`"Enhanced Software Inventory`">`n<TablixDeviceDetail>`n<GroupingDetails_Collection>`n" | Out-File $assurx 

##################
## Begin Engine ##
##################

Write-Host "** Begin Array Node Name **"

$nodeArray = Get-Content $nodeListFile

ForEach ($node in $nodeArray )
 {
  
    ## Parsing & Retrieving Data

    #DEBUG-MODE Uncomment Line 39 and Comment Out Line 40
    #$elementReport | Select-Xml -XPath "//ReportOutput/ReportBody/ReportSection[@name='$node']//String"|%{$_.Node.'#text'} | Out-File "$temp\$node-output.txt"; "END" | Out-File -Append $temp\$node-output.txt
    $elementReport | Select-Xml -XPath "//ReportOutput/ReportBody/ReportSection[@name='$node']//String"|%{$_.Node.'#text'} | Out-File "$temp\output.txt"; "END" | Out-File -Append $temp\output.txt
    $elementReport | Select-Xml -XPath "//ReportOutput/ReportBody/ReportSection[@name='$node']/ReportSection/ReportSection/ReportSection" | Select-Object –ExpandProperty “node” | select name | foreach-object{$_.name} | Out-File $temp\time.txt
  
    ## Grab the DateTimeStamp & Format Accordingly
    $time = Get-Content $temp\time.txt
    $time = $time.replace(' ','T')  
    $time = $time.Substring(0,$time.Length-2)
     
    #DEBUG MODE - Uncomment Line 48 and Comment Out Line 49 - 
    #$lineArray = Get-Content $temp\$node-output.txt | Select-Object -Skip 6 |Foreach {$_.TrimEnd()} | Set-Content $temp\newOutput.txt
     $lineArray = Get-Content $temp\output.txt | Select-Object -Skip 6 |Foreach {$_.TrimEnd()} | Set-Content $temp\newOutput.txt
     $lineArray2 = Get-Content $temp\newOutput.txt
    
    # Grab the OS Version and OS Platform
    ForEach ($line in $lineArray2 ) {
    If ($line.StartsWith("OS:")) { $OS = $line.Substring(6) } 
    If ($line.StartsWith("Version:")) { $OSVersion = $line.Substring(10) } 
    } 

    # Remove the 2 OS Version and OS Platform from the working file
    $lineArray2 = Get-content $temp\newOutput.txt | select-string -pattern 'OS:|Version:' -notmatch | Out-File $temp\newOutput2.txt
    $lineArray2 = Get-content $temp\newOutput2.txt 

    ## Begin Node Output to FINAL xml File
    "<GroupingDetails Asset=`"$node`" Location=`" `">`n<subSoftwareInventory>`n<Report Name=`"subSoftwareInventory`">`n<table1>`n<Detail_Collection>`n<Detail Time=`"$time`" HostPropertyValue=`"$OS`" PropertyName=`"Operating System`"/>`n<Detail Time=`"$time`" HostPropertyValue=`"$OSVersion`" PropertyName=`"Operating System Version`"/>`n</Detail_Collection>`n</table1>"| Out-File -Append $assurx
   
    ## CODE FOR RETRIEVING DATA
    "`n<Tablix1>`n<Details_Collection>" | Out-File -Append $assurx



For ($i=0; $i -lt $lineArray2.Length; $i++) 
 {

  If ($lineArray2[$i].StartsWith("***")) 
   { 
               
     ## Check to see if Software Name Wraps to next line
     If ($lineArray2[$i+1].length -eq 80)
      { $nextLine = $lineArray2[$i + 2]
        $retrieve48 = $nextLine.substring(0,48)  
        $softwareName = $lineArray2[$i+1] + $retrieve48
       }

     ## Check to see if Software Name does not wrap to next line
     If ($lineArray2[$i+1].length -ne 80)
      { $softwareName = $lineArray2[$i+1]
       }

     ##Check to see if Version exists
     If ($lineArray2[$i+2].substring(17,1) -match " ") {$version = $lineArray2[$i+2].substring(49)}
     
     ##Check to see if Version does not exists
     If ($lineArray2[$i+2].substring(17,1) -notmatch " ") 
      { 
       
       ##Check to see if Vendor exists or not
       If ($lineArray2[$i+2].substring(17,1) -notmatch " "){ $vendor = $lineArray2[$i+2].substring(17)}
       If ($lineArray2[$i+2].substring(17,1) -match " ") { 
       
       ## Check to see if Installed Date does not exists for an element without Version or is at the end of the file
       If ($lineArray2[$i+3] -eq "***" -or $lineArray2[$i+3] -eq "END"){ $installedDate = "" }
       If ($lineArray2[$i+3] -ne "***" -AND $lineArray2[$i+3] -ne "END"){ $installedDate = $lineArray2[$i+3].substring(65) }
            
       continue      
      }
      }

     ##Check to see if Vendor exists
     If ($lineArray2[$i+3].substring(17,1) -notmatch " ")
     { $vendor = $lineArray2[$i+3].substring(17) }

     ##Check to see if Vendor does not exists
     If ($lineArray2[$i+3].substring(17,1) -match " ")
     { 

      ## Check to see if Installed Date does not exists for an element without Vendor
      If ($lineArray2[$i+3] -eq "***" -or $lineArray2[$i+3] -eq "END") { $installedDate = "" }
      If ($lineArray2[$i+3] -ne "***" -AND $lineArray2[$i+3] -ne "END") { $installedDate = $lineArray2[$i+3].substring(65) }
            
      continue
     }

     ## Check to see if Installed Date does not exists or is at the end of the file
     If ($lineArray2[$i+4] -eq "***" -or $lineArray2[$i+4] -eq "END")
     { $installedDate = "" }
     
     ## Check to see if Installed Date exists 
     If ($lineArray2[$i+4] -ne "***" -AND $lineArray2[$i+4] -ne "END")
     { $installedDate = $lineArray2[$i+4].substring(65) }
    
    "`n<Details Name=`"$softwareName`" CollectionTime=`"$time`" InstalledDate=`"$installedDate`" Vendor=`"$vendor`" Version=`"$version`"/> " | Out-File -Append $assurx
       
    }

   
    }

   } 

    ## Close Node Output to Final xml File
    "`n</Details_Collection>`n</Tablix1>`n</Report>`n</subSoftwareInventory>`n</GroupingDetails>" | Out-File -Append $assurx
  
  
  Write-Host "** Finishing Parsing the XML **"

########################
## Close Out XML File ##
########################

"`n</GroupingDetails_Collection>`n</TablixDeviceDetail>`n</Report>" | Out-File -Append $assurx

(gc $assurx )| ? {$_.trim() -ne "" } | set-content $assurx 
(gc $assurx ) | out-file -encoding ASCII $assurx

 
#######################################################################  
## Timestamp the working files and move them to the backup directory ##              
#######################################################################  
Write-Host "** Moving & Timestamping Working Files **"

$timer = (Get-Date -Format MM-dd-yyy-hhmm)
Get-ChildItem -Path $assurx | Copy-Item -Destination "$backup"
Get-ChildItem -Path $backup\assurx.xml | Rename-Item -NewName {$_.BaseName+'_'+$timer+$_.Extension}

Write-Host "** Assurx Backup Report has been timestamped and moved to: $backup **"